//
//  AppDelegate.swift
//  2LemetryTest
//
//  Created by Avnish Chuchra on 04/03/16.
//  Copyright © 2016 Avnish Chuchra. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,  MQTTConnectionStatusListener, MQTTMessageListener {

    var window: UIWindow?
    var a_MQTTClient:MQTTClient

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        
        
    }
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        
        
    }
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
    }
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        
        
    }
    func setupMQTTClient (){
        a_MQTTClient.setupMQTT(<#T##host: String##String#>, port: 1883, clientID: <#T##String#>, username: <#T##String#>, password: <#T##String#>, keepAlive: 60, clean: false, topic: <#T##String#>, message: <#T##NSData#>, qosLevel: 2, retainFlag:true )
        //[self.a_MQTTClient setupMQTT:[NSString stringWithFormat:@"%@",finalUrlStr] port:1883 clientID:self.clientIDStr username:userName password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"] keepAlive:60 clean:FALSE topic:strWillTopic message:data qosLevel:2 retainFlag:TRUE];
        
    }
    func Add2LementryListener() {
        a_MQTTClient.addMQTTConnectionStatusListener(self)
        a_MQTTClient.addMQTTMessageListener(self)
    }
    func MQTTSessionConnected() {
        print("MQTTSessionConnected")
        //self.performSelector(@selector ->subscribedata), withObject: nil, afterDelay: 60)
        self.subscribedata()
        
    }
    func MQTTConnectionRefused() {
        self.disconnect2Lementry()
    }
    func subscribedata() {
        
    }
    
//    
//    
//    //MARK: MQTT Listener Callback methods
//    
//    -(void) MQTTSessionConnected  {
//    NSLog(@"MQTTSessionConnected");
//    [Crittercism endTransaction:@"2lemetryConnect"];
//    self.connecting = true;
//    self.counterTimer = 0;
//    
//    if (self.TwoLementryFectch)
//    {
//    self.TwoLementryFectch = FALSE;
//    [self PushRQRS:@"N"];
//    [self performSelector:@selector(subscribedata) withObject:nil afterDelay:60.0];
//    }
//    else
//    {
//    [self subscribedata];
//    }
//    
//    
//    
//    }
//    
//    -(void) MQTTConnectionRefused {
//    NSLog(@"MQTTConnectionRefused");
//    [Crittercism failTransaction:@"2lemetryConnect"];
//    self.connecting = false;
//    [self disconnect2Lementry];
//    
//    
//    }
//  
    
    func MQTTConnectionClosed() {
        self.disconnect2Lementry()
    }
    
//    -(void) MQTTConnectionClosed {
//    NSLog(@"MQTTConnectionClosed");
//    [Crittercism failTransaction:@"2lemetryConnect"];
//    self.connecting = false;
//    [self disconnect2Lementry];
//    }
//    
    
    func MQTTConnectionError() {
         self.disconnect2Lementry()
    }
//    -(void) MQTTConnectionError {
//    NSLog(@"MQTTConnectionError");
//    [Crittercism failTransaction:@"2lemetryConnect"];
//    self.connecting = false;
//    // [self setupMQTTClient];
//    [self disconnect2Lementry];
//    
//    }
//    
    func disconnect2Lementry() {
        self.setupMQTTClient()
    }
    
//    -(void)disconnect2Lementry
//    {
//    if (self.counterTimer >= 10)
//    {
//    //        if (self.timer)
//    //        {
//    if (self.timer!=nil)
//    {
//    [self.timer invalidate];
//    self.timer = nil;
//    }
//    
//    //            [self performSelector:@selector(stopTimer) withObject:nil afterDelay:15.0];
//    //
//    //        }
//    self.counterTimer = 0;
//    UIAlertView *alert1 = [[UIAlertView alloc] initWithTitle:@"Unable to connect to 2lemetry" message:@"Please retry or contact your Fleet Department for assistance" delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:@"Exit", nil];
//    alert1.tag = 7777;
//    alert1.delegate = self;
//    [alert1 show];
//    
//    }
//    else
//    {
//    
//    if (self.timer)
//    {
//    [self.timer invalidate];
//    self.timer = nil;
//    }
//    [self performSelector:@selector(setupMQTTClient) withObject:nil afterDelay:5.0];
//    // [self setupMQTTClient];
//    self.counterTimer++;
//    }
//    
//    }
//    
//    //-(void) applicationDidEnterBackground:(UIApplication *)application
//    //{
//    //    UIBackgroundTaskIdentifier taskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^(void) {
//    //
//    //        if (taskId != UIBackgroundTaskInvalid) {
//    //            [[UIApplication sharedApplication] endBackgroundTask:taskId];
//    //        }
//    //    }];
//    //
//    //}
//    
//    // MARK: Background task
//    
//    //-(void) appDidEnterBackground (){
//    //    var bti : UIBackgroundTaskIdentifier = 0
//    //    bti = UIApplication.sharedApplication()
//    //    .beginBackgroundTaskWithExpirationHandler({
//    //        UIApplication.sharedApplication().endBackgroundTask(bti)
//    //        bti = UIBackgroundTaskInvalid
//    //    })
//    //
//    //}
//    
//    //MARK: New message received
//    
    
    func MQTTMessageReceived(message: String, topic: String) {
        let receivedData = (message as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        var error:NSError? = nil
        if let jsonObject: AnyObject = NSJSONSerialization.JSONObjectWithData(receivedData!, options: nil) {
            if let dict = jsonObject as? NSDictionary {
                print(dict)
            } else {
                print("not a dictionary")
            }
        } else {
            print("Could not parse JSON: \(error!)")
        }
    }
    
//    - (void)MQTTMessageReceived:(NSString * )message topic:(NSString * )topic
//    {
//    NSLog(@"MQTTMessageReceived");
//    NSLog(@"message %@",message);
//    
//    NSString *ViewController = NSStringFromClass([self.navController.topViewController class]);
//    if (![ViewController isEqualToString:@"LoginViewController"])
//    {
//    [Crittercism beginTransaction:@"Message Received"];
//    
//    NSData* data = [message dataUsingEncoding:NSUTF8StringEncoding];
//    
//    [Crittercism endTransaction:@"Message Received"];
//    [self PushRQRSNotify:data];
//    
//    if (self.isInBackgound)
//    {
//    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:1];
//    localNotification.timeZone = [NSTimeZone defaultTimeZone];
//    
//    localNotification.alertBody = @"You recieved a new notification";
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//    
//    }
//    }
//    
//    
//    NSString *topicStr = [NSString stringWithFormat:@"%@/%@/%@",[self.configDec valueForKey:@"TW_CONFIG_NA_THING_FABRIC_DOMAIN"],[self.configDec valueForKey:@"TW_CONFIG_NA_THING_FABRIC_STUFF"],[[self.ChauffeurNumber stringByAppendingString:@"@"]stringByAppendingString:self.providerCode]];
//    
//    // if (self.internetActive)
//    if (![ViewController isEqualToString:@"LoginViewController"]) {
//    NSData* data = [message dataUsingEncoding:NSUTF8StringEncoding];
//    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] initWithDictionary:[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil]];
//    [jsonDict setObject:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:@"NotificationRecivedTime"];
//    
//    NSData* data1 = [NSData new];
//    if ([NSJSONSerialization isValidJSONObject:jsonDict]) {
//    data1 = [ NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:nil];
//    }
//    [self.a_MQTTClient publishData:data1 topic:topicStr];
//    }
//    
//    }
//    -(void) setupMQTTClient
//    {
//    if (!self.connecting) {
//    [Crittercism beginTransaction:@"2lemetryConnect"];
//    
//    NSString *urlStr = [NSString stringWithFormat:@"%@",[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_URL"]];
//    NSString* finalUrlStr = [urlStr stringByReplacingOccurrencesOfString:@"tcp://" withString:@""];
//    NSArray *keys = [NSArray arrayWithObjects:@"Disconnected", nil];
//    NSString* msgStr = [NSString stringWithFormat:@"User %@@%@ has lost connection",self.ChauffeurNumber,self.providerCode];
//    
//    NSArray *objects = [NSArray arrayWithObjects:msgStr, nil];
//    
//    NSDictionary *arrayOfObjects = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
//    
//    NSData* data;
//    NSString *str;
//    str = @"USFT_TopicLastWill";
//    if ([NSJSONSerialization isValidJSONObject:arrayOfObjects])
//    {
//    data = [ NSJSONSerialization dataWithJSONObject:arrayOfObjects options:NSJSONWritingPrettyPrinted error:nil ];
//    //str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    
//    }
//    self.clientIDStr = [NSString stringWithFormat:@"%@@%@",self.ChauffeurNumber,self.providerCode];
//    NSString*userName = [self.configDec objectForKey:@"TW_CONFIG_NA_USFT_USER_NAME"];
//    
//    NSString *strWillTopic = [NSString stringWithFormat:@"%@/%@/%@",[self.configDec valueForKey:@"TW_CONFIG_NA_THING_FABRIC_DOMAIN"],[self.configDec valueForKey:@"TW_CONFIG_NA_THING_FABRIC_STUFF"],str];
//    // NSLog(@"strWillTopic====%@",strWillTopic);
//    
//    // NSString*strWillTopic = @"fyuas60m8pbgqh9/things/USFT_TopicLastWill";
//    if (!self.fisrtLogin)
//    {
//    //  [self.a_MQTTClient setupMQTT:[NSString stringWithFormat:@"%@",finalUrlStr] port:1883 clientID:self.clientIDStr username:userName password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"]];
//    
//    [self.a_MQTTClient setupMQTT:[NSString stringWithFormat:@"%@",finalUrlStr] port:1883 clientID:self.clientIDStr username:userName password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"] keepAlive:60 clean:TRUE topic:strWillTopic message:data qosLevel:2 retainFlag:TRUE];
//    
//    //           self.mqttSession = [[MQTTSession alloc] initWithAClientId:self.clientIDStr aUserName:userName Password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"] aKeepAliveInterval:60 aCleanSessionFlag:YES willTopic:strWillTopic willMsg:data willQoS:2 willRetainFlag:TRUE] ;
//    }
//    else
//    {
//    //  [self.a_MQTTClient setupMQTT:[NSString stringWithFormat:@"%@",finalUrlStr] port:1883 clientID:self.clientIDStr username:userName password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"]];
//    
//    [self.a_MQTTClient setupMQTT:[NSString stringWithFormat:@"%@",finalUrlStr] port:1883 clientID:self.clientIDStr username:userName password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"] keepAlive:60 clean:FALSE topic:strWillTopic message:data qosLevel:2 retainFlag:TRUE];
//    
//    //            self.mqttSession = [[MQTTSession alloc] initWithAClientId:self.clientIDStr aUserName:userName Password:[self.configDec objectForKey:@"TW_CONFIG_NA_THING_FABRIC_MOBILE_PASSWORD"] aKeepAliveInterval:60 aCleanSessionFlag:NO willTopic:strWillTopic willMsg:data willQoS:2 willRetainFlag:TRUE] ;
//    }
//    
//    }
//    
//    }

}

